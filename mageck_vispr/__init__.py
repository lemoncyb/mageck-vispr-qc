__author__ = "Johannes Köster"
__copyright__ = "Copyright 2015, Johannes Köster, Liu lab"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"

import os
import glob
import shutil
import re    #[cuiyb]++
from collections import defaultdict

import yaml

from mageck_vispr.check_config import check_config, ConfigError


COMBAT_SCRIPT_PATH = os.path.join(os.path.dirname(__file__), "combat.R")


def get_norm_method(config):
    if "norm_method" in config:
        return config["norm_method"]
    elif "counts" in config or "batchmatrix" in config:
        return "none"
    else:
        return "median"


def get_counts(config, normalized=False):
    if "batchmatrix" in config:
        return "results/count/all.count.batchcorrected.txt"
    else:
        suffix = "_normalized" if normalized else ""
        return config.get("counts", "results/count/all.count{}.txt".format(suffix))


def annotation_available(config):
    return "library" in config and config["assembly"] in [
        "mm9", "mm10", "hg19", "hg38"] and config["sgrnas"]["len"] in [19, 20]


def design_available(config):
    if re.search('\/dev\/null', str(config["experiments"])):
        return False
    elif "day0label" in config:
        raise ConfigError("Users can either specify a day0label (with an empty design matrix), or a design matrix file, but can't specify the both.")
    else:
        return True


def efficiency_estimation_available(config, experiment):
    return "designmatrix" in config["experiments"][experiment]


def postprocess_config(config):
    check_config(config)
    config["replicates"] = {}
    if "samples" in config:
        if not "library" in config:
            raise ConfigError("A library file has to be specified in the configuration.")
        for sample in config["samples"]:
            replicates = config["samples"][sample]
            if not isinstance(replicates, list):
                replicates = [replicates]
            config["samples"][sample] = []
            for i, replicate in enumerate(replicates):
                name = "{}_{}".format(sample, i)
                config["replicates"][name] = replicate
                config["samples"][sample].append(name)


def get_fastq(replicate, config):
    if "adapter" in config["sgrnas"]:
        return "results/trimmed_reads/{}.fastq".format(replicate)
    return config["replicates"][replicate]


def vispr_config(input, output, wildcards, config):
    relpath = lambda path: os.path.relpath(path, "results")
    copy = lambda path: shutil.copy(path, "results")
    vispr_config = {
        "experiment": wildcards.experiment,
        "species": config["species"],
        "assembly": config["assembly"],
        "targets": {
            "results": relpath(input.results),
            "genes": config["targets"]["genes"]
        },
        "sgrnas": {
            "counts": relpath(input.counts)
        }
    }
    if "fastqc" in input.keys():
        samples = {
            rep: sample
            for sample, replicates in config["samples"].items()
            for rep in replicates
        }
        qc = defaultdict(list)
        for replicate, fastqc in zip(config["replicates"], input.fastqc):
            qc[samples[replicate]].extend(
                relpath(os.path.join(data, "fastqc_data.txt"))
                for data in sorted(glob.iglob("{}/*_fastqc".format(fastqc))))
        vispr_config["fastqc"] = dict(qc)
    if "mapstats" in input.keys():
        vispr_config["sgrnas"]["mapstats"] = relpath(input.mapstats)
    if "controls" in config["targets"]:
        copy(config["targets"]["controls"])
        vispr_config["targets"]["controls"] = os.path.basename(
            config["targets"]["controls"])
    if annotation_available(config):
        copy("annotation/sgrnas.bed")
        vispr_config["sgrnas"]["annotation"] = "sgrnas.bed"
    if efficiency_estimation_available(config, wildcards.experiment):
        vispr_config["sgrnas"]["results"] = relpath(input.sgrna_results)
    with open(output[0], "w") as f:
        yaml.dump(vispr_config, f, default_flow_style=False)
