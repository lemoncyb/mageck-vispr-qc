__author__ = "Johannes K�ster"
__copyright__ = "Copyright 2015, Johannes K�ster, Liu lab"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"

import argparse
import logging
import sys
import os
import shutil
import jinja2

import yaml

from mageck_vispr.version import __version__
from mageck_vispr import annotation


def init_workflow(directory, reads, keep_config=False):
    try:
        os.makedirs(directory)
    except OSError:
        # either directory exists (then we can ignore) or it will fail in the
        # next step.
        pass

    def guess_sample_name(f):
        if f.endswith(".fastq"):
            return os.path.splitext(os.path.basename(f))[0]
        elif f.endswith(".fastq.gz"):
            return os.path.splitext(
                os.path.splitext(os.path.basename(f))[0])[0]
        else:
            logging.error("Reads must be given as .fastq or .fastq.gz files.")

    # [cuiyb]++ identify length of sgRNA from library file
    def identify_sgrnas_len(dir):
        with open("./lib/library.csv", "r") as f:
            line = f.readline()    # skip file header
            line = f.readline()
            line = line.split(',')
            return len(line[1])

    # [cuiyb]++ extract PMID from path
    #def extract_pmid():
        #path, base = os.path.split(os.getcwd())
        #pmid = base[0:8]    # extract PMID from dir name '24535568_YusaK_NatBiotechnol_2014'
        #while not base.isdigit():
        #    path, base = os.path.split(path)
    #    return path, base

    # [cuiyb]++
    def identify_species():
        path, base = os.path.split(os.getcwd())
        index_file = os.path.join(path, "CRISPRScreenData.txt")
        with open(index_file, "r") as f:
            line = f.readline()    # skip file header
            line = f.readline()
            line = line.split('\t')
            while line[1] != base:
                line = f.readline()
                line = line.split('\t')
            if line[5] == "human":
                return "homo_sapiens", "hg38"
            if line[5] == "mouse":
                return "mus_musculus", "mm10"

    # [cuiyb]++ extract day0-label from mle design matrix
    #def extract_day0():
    #    with open("./designmatrix/designmatrix.txt", "r") as fin:
    #        line = fin.readline()  # skip file header
    #        line = fin.readline()
    #        day0_list = []
    #        while line:
    #            line = line.split('\t')
    #            line[-1] = line[-1][:-1]  # del the line end
    #            tag = 0
    #            for x in line:
    #                if x == '1':
    #                    tag = tag + 1
    #            if tag == 1:
    #                day0_list.append(line[0])
    #            line = fin.readline()
    #    # convert label list to label string seperated with comma
    #    day0_labels = ""
    #    for x in day0_list:
    #        day0_labels = day0_labels + "," + x
    #    day0_labels = day0_labels[1:]    # del the first char ','
    #    return day0_labels

    # [cuiyb]++ extract day0-label from PublicCRISPRScreenData.txt
    def extract_day0():
        path, base = os.path.split(os.getcwd())
        index_file = os.path.join(path, "CRISPRScreenData.txt")
        with open(index_file, "r") as f:
            line = f.readline()  # skip file header
            line = f.readline()
            line = line.split('\t')
            while line[1] != base:
                line = f.readline()
                line = line.split('\t')
        return line[6]  # Initial_Condition cloumn

    def get_resource(f):
        return os.path.join(os.path.dirname(__file__), f)

    def get_target(f):
        return os.path.join(directory, f)

    def backup(f):
        target = get_target(f)
        if os.path.exists(target):
            shutil.copy(target, target + ".old")

    def install(f, backup=True):
        source = get_resource(f)
        target = get_target(f)
        shutil.copy(source, target)

    backup("Snakefile")
    install("Snakefile")
    install("README.txt")
    if not keep_config:
        backup("config.yaml")
        with open(get_resource("config.yaml")) as f:
            config_template = jinja2.Template(f.read(),
                                              trim_blocks=True,
                                              lstrip_blocks=True)
        samples = None
        if reads is not None:
            samples = {
                guess_sample_name(f): os.path.relpath(f, directory)
                for f in reads
            }
        sgrnas_length = identify_sgrnas_len(directory)    # [cuiyb]++ get length of sgRNA sequence
        day0 = extract_day0()    # [cuiyb]++ extract day0-label from MLE design matrix
        species, assembly = identify_species()    # [cuiyb]++
        with open(get_target("config.yaml"), "w") as out:
            out.write(config_template.render(
                                            samples=samples,
                                            sgrnas_len=sgrnas_length,
                                            species=species,
                                            assembly=assembly,
                                            day0label=day0))


def annotate_library(library,
                     sgrna_len=None,
                     assembly=None,
                     annotation_table=None):
    if annotation_table is None and sgrna_len and assembly:
        annotation_table = (
            "https://bitbucket.org/liulab/mageck-vispr/"
            "downloads/sgrna_annotation_{assembly}_exome_{len}bp.txt.bz2"
        ).format(assembly=assembly,
                 len=sgrna_len)
    a = annotation.Annotator(library, annotation_table)
    a.annotate()


def main():
    # create arg parser
    parser = argparse.ArgumentParser(
        "MAGeCK-VISPR-QC is a comprehensive quality control, analysis and "
        "visualization pipeline for CRISPR/Cas9 screens.")
    parser.add_argument("--version",
                        action="store_true",
                        help="Print version info.")
    subparsers = parser.add_subparsers(dest="subcommand")

    workflow = subparsers.add_parser(
        "init",
        help="Initialize the MAGeCK/VISPR workflow "
        "in a given directory. This will "
        "install a Snakefile, a README and a "
        "config file in this directory. "
        "Configure the config file according "
        "to your needs, and run the workflow "
        "with Snakemake "
        "(https://bitbucket.org/johanneskoester/snakemake).")
    workflow.add_argument("directory",
                          help="Path to the directory where the "
                          "workflow shall be initialized.")
    workflow.add_argument("--reads",
                          nargs="+",
                          help="Paths to FastQ files with reads that shall be "
                          "added to the config file. You can edit the sample "
                          "sample names and assignment to experiments "
                          "in the config file.")
    workflow.add_argument("--keep-config", action="store_true",
                          help="Keep existing config file.")

    annotate = subparsers.add_parser(
        "annotate-library",
        help="Annotate an sgRNA library design with information about "
        "sgRNA position and predicted efficiency. Annotation is printed in "
        "BED format.")
    annotate.add_argument(
        "library",
        help="Path to sgRNA library design file (comma separated, columns "
        "identifier, sequence, gene).")
    annotate.add_argument("--sgrna-len",
                          type=int,
                          choices=[19, 20],
                          help="Length of sgrnas in library file.")
    annotate.add_argument("--assembly",
                          choices=["mm10", "mm9", "hg38", "hg19"],
                          help="Assembly to use.")
    annotate.add_argument(
        "--annotation-table",
        help="As an alternative to specifying the sgrna length and assembly, "
        "a path to an annotation table can be provided "
        "(tab separated, no header; with columns chromosome, "
        "start, end, gene, score, strand, sequence). This can also be a URL. "
        "See https://bitbucket.org/liulab/mageck-vispr/downloads for precomputed tables.")

    logging.basicConfig(format="%(message)s",
                        level=logging.INFO,
                        stream=sys.stderr)

    args = parser.parse_args()

    if args.version:
        print(__version__)
        exit(0)
    elif args.subcommand == "init":
        init_workflow(args.directory, args.reads, keep_config=args.keep_config)
    elif args.subcommand == "annotate-library":
        if (args.assembly and not args.sgrna_len) or (
            args.sgrna_len and not args.assembly
        ) or not (args.sgrna_len or args.assembly or args.annotation_table):
            parser.print_help()
            print(
                "Error: Either specify path to an annotation table or specify --sgrna-len and --assembly.")
            exit(1)
        annotate_library(args.library,
                         assembly=args.assembly,
                         sgrna_len=args.sgrna_len,
                         annotation_table=args.annotation_table)
    else:
        parser.print_help()
        exit(1)
    exit(0)
